package com.criapp.circleprogressview;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.criapp.circleprogresscustomview.CircleProgressView;

public class MainActivity extends AppCompatActivity {
    TextView tvProgress, tvProgressOnLongClick;
    CircleProgressView iv, iv1, iv2, longClickBtn;
    CircleProgressView laps[] = new CircleProgressView[3];
    TextView lapsText[] = new TextView[3];
    int lap2progress = 20, lap1progress = 99, lap3progress = 69;
    final int fullProgress = 270;//3.6 is full circle
    CircleProgressView.CircleProgressCallback callback, longClickCallback;
    Button btnAnimate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setProgressViews();
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        simulateProgress();
    }

    private void setProgressViews() {
        tvProgress = (TextView) findViewById(R.id.tvPercentMain);
        tvProgressOnLongClick = (TextView) findViewById(R.id.tvPercent0);
        longClickBtn = (CircleProgressView) findViewById(R.id.vButton0);
        iv2 = (CircleProgressView) findViewById(R.id.vButton);
        iv1 = (CircleProgressView) findViewById(R.id.vButton1);
        iv = (CircleProgressView) findViewById(R.id.vButton2);
        laps[0] = (CircleProgressView) findViewById(R.id.vButtonLap1);
        laps[1] = (CircleProgressView) findViewById(R.id.vButtonLap2);
        laps[2] = (CircleProgressView) findViewById(R.id.vButtonLap3);
        lapsText[0] = (TextView) findViewById(R.id.tvPlap1);
        lapsText[1] = (TextView) findViewById(R.id.tvPlap2);
        lapsText[2] = (TextView) findViewById(R.id.tvPlap3);
        btnAnimate = (Button) findViewById(R.id.btnAnimate);

        prepareStaticCircleProgressView(0, R.color.light_orange, lap1progress);
        prepareStaticCircleProgressView(1, R.color.burgundy, lap2progress);
        //prepareStaticCircleProgressView(2, R.color.blue, lap3progress); // this one is prepared in xml

        prepareCircleProgressView(iv, R.color.light_orange);
        prepareCircleProgressView(iv1, R.color.burgundy);
        prepareCircleProgressView(iv2, R.color.blue);
    }

    private void setListeners() {
        for (int i = 0; i < 3; i++)
            laps[i].setOnLongClickListener(null);// just show static progress
        tvProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simulateProgress();
            }
        });
        btnAnimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simulateProgress();
            }
        });
        setCircleCallback();
        longClickBtn.setmCallback(longClickCallback);
        longClickBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //reset view
                tvProgressOnLongClick.setText(getString(R.string.long_click_msg));
                longClickBtn.setInit(true);
                longClickBtn.invalidate();
            }
        });
    }

    private void simulateProgress() {
        btnAnimate.setVisibility(View.GONE);
        int progress1 = lap1progress * fullProgress / 100;
        int progress2 = lap2progress * fullProgress / 100;
        int progress3 = lap3progress * fullProgress / 100;

        if (progress1 >= progress2 && progress1 >= progress3) {
            iv.setmCallback(callback);
        } else if (progress2 >= progress1 && progress2 >= progress3) {
            iv1.setmCallback(callback);
        } else
            iv2.setmCallback(callback);
//        iv2.setProgressPeriodMills(60);// 2 times slower animation
//        iv.setProgressPeriodMills(45);// default speed is 30
        iv2.simulateProgress(progress3);
        iv1.simulateProgress(progress2);
        iv.simulateProgress(progress1);

    }

    private void prepareCircleProgressView(CircleProgressView iv, int color) {
        iv.setTopProgress(fullProgress);// 270 is 3/4 of circle
        iv.setColorProgress(ContextCompat.getColor(this, color));
        iv.setColorDone(Color.TRANSPARENT);
        iv.setColorWrong(Color.TRANSPARENT);
        iv.setColorBaseFill(Color.TRANSPARENT);
        iv.setColorBaseStroke(Color.TRANSPARENT);
        iv.setColorWrongBaseStroke(Color.TRANSPARENT);
//        iv.setStrokeWidth(5);
        iv.setStartProgress(90);//starts from bottom
    }

    private void prepareStaticCircleProgressView(int pos, int color, int progress) {
        if (progress >= 100)
            lapsText[pos].setTextColor(ContextCompat.getColor(this, R.color.white));
        lapsText[pos].setText(progress + "%");
        laps[pos].setColorProgress(ContextCompat.getColor(this, color));
        laps[pos].setColorDone(ContextCompat.getColor(this, R.color.green_done));
        laps[pos].setColorBaseStroke(ContextCompat.getColor(this, R.color.grey));
        laps[pos].setColorBaseFill(Color.TRANSPARENT);
        laps[pos].setProgress((int) (progress * 3.6));//100% = 360
        laps[pos].setInit(false); // important to redraw without click event
        laps[pos].setTopProgress(360);// full circle
        iv.setStartProgress(-90);//starts from top
        laps[pos].invalidate();
    }

    private void setCircleCallback() {
        callback = new CircleProgressView.CircleProgressCallback() {
            @Override
            public void onProgress(int progress1) {
                tvProgress.setText(Integer.toString(progress1));
            }

            @Override
            public void onFinish(int progress1) {
                btnAnimate.setVisibility(View.VISIBLE);
                tvProgress.setText(Integer.toString(progress1));
            }

            @Override
            public void onError(int progress1) {
                btnAnimate.setVisibility(View.VISIBLE);
                tvProgress.setText(Integer.toString(progress1));
            }
        };
        longClickCallback = new CircleProgressView.CircleProgressCallback() {

            @Override
            public void onProgress(int progress1) {
                tvProgressOnLongClick.setText(progress1 + "%");
            }

            @Override
            public void onFinish(int progress1) {
                tvProgressOnLongClick.setText(progress1 + "%-finished\n" +
                        "click the circle to reset");
            }

            @Override
            public void onError(int progress1) {
                tvProgressOnLongClick.setText(progress1 + "%-error\n" +
                        "click the circle to reset");
            }
        };
    }
}
